const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

const db = admin.database();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((req, res) => {
//     res.set('Access-Control-Allow-Origin', '*');
//     console.log(req.method);
//     res.send("hello world");
// });

exports.getdonor = functions.https.onRequest((request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    var latitude = parseFloat(request.query.lat);
    var longitude = parseFloat(request.query.long);
    var uid = request.query.uid;
    var group = request.query.bgroup;

    console.log(uid);

    var ref = db.ref('/blooddonors');
    var responsedata = {'status':200,
                        'donors':[]};

    var compatablegroups;
    switch (group)
    {
        case 'A+':
            compatablegroups = ['O+','O-','A+','A-']
            break;
        case 'A-':
            compatablegroups = ['A-','O-'];
            break;
        case 'B+':
            compatablegroups = ['B+','B-','O+','O-'];
            break;
        case 'B-':
            compatablegroups = ['B-','O-'];
            break;
        case 'AB+':
            compatablegroups = ['A+','A-','B+','B-','O+','O-','AB+','AB-'];
            break;
        case 'AB-':
            compatablegroups = ['AB-','A-','B-','O-'];
            break;
        case 'O+':
            compatablegroups = ['O+', 'O-'];
            break;
        case 'O-':
            compatablegroups = ['O-'];
            break;
    }
    // for (var i = 0 ; i < 3 ; ++i){
    //     responsedata.donors.push({'id':i,'name':'navaneeth'});
    // }

    // response.json(responsedata);

    ref.on('value', function(snapshot){
        var donorslist = snapshot.val();
        var selecteddonorslist = [];
    
        for(var key in donorslist){
            if(compatablegroups.indexOf(donorslist[key]['bloodgroup']) >= 0 && donorslist[key]['available'] ){
                var date = new Date();
                var weekday = new Array(7);
                weekday[0] = "sun";
                weekday[1] = "mon";
                weekday[2] = "tue";
                weekday[3] = "wed";
                weekday[4] = "thu";
                weekday[5] = "fri";
                weekday[6] = "sat";
                
                var nowstamp = 1000 * date.getHours() + date.getMinutes()
                var fromstamp = 1000 * donorslist[key]['fromhour'] + donorslist[key]['frommin']
                var tostamp = 1000 * donorslist[key]['tohour'] + donorslist[key]['tomin']

                console.log(nowstamp + " now " + typeof(nowstamp))
                console.log(fromstamp + " from " + typeof(fromstamp))
                console.log(tostamp + " to " + typeof(tostamp))

                if(donorslist[key][weekday[date.getDay()]]==true)
                {
                    if (nowstamp > fromstamp && nowstamp < tostamp)
                    {
                        selecteddonorslist.push(donorslist[key]);
                    }
                }
            }
        }
        for(var key in selecteddonorslist){
            responsedata.donors.push({
                'name':selecteddonorslist[key]['name'],
                'mobile':selecteddonorslist[key]['contactno'],
                'distance':distance(latitude,longitude,selecteddonorslist[key]['latitude'],selecteddonorslist[key]['longitude'],'K') + " km",
                'bgroup':selecteddonorslist[key]['bloodgroup']
            })
        }
        responsedata.donors.sort((a, b) => parseFloat(a.distance) - parseFloat(b.distance));
        response.json(responsedata);
        return;
    },function(errorobject){
        response.json({
            'status': 404,
            'donors': []
        });
        return;
    });
});

exports.getbrutedonor = functions.https.onRequest((request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    var latitude = parseFloat(request.query.lat);
    var longitude = parseFloat(request.query.long);
    var uid = request.query.uid;
    var group = request.query.bgroup;

    console.log(uid);

    var ref = db.ref('/blooddonors');
    var responsedata = {
        'status': 200,
        'donors': []
    };

    var compatablegroups;
    switch (group) {
        case 'A+':
            compatablegroups = ['O+', 'O-', 'A+', 'A-']
            break;
        case 'A-':
            compatablegroups = ['A-', 'O-'];
            break;
        case 'B+':
            compatablegroups = ['B+', 'B-', 'O+', 'O-'];
            break;
        case 'B-':
            compatablegroups = ['B-', 'O-'];
            break;
        case 'AB+':
            compatablegroups = ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-'];
            break;
        case 'AB-':
            compatablegroups = ['AB-', 'A-', 'B-', 'O-'];
            break;
        case 'O+':
            compatablegroups = ['O+', 'O-'];
            break;
        case 'O-':
            compatablegroups = ['O-'];
            break;
    }
    // for (var i = 0 ; i < 3 ; ++i){
    //     responsedata.donors.push({'id':i,'name':'navaneeth'});
    // }

    // response.json(responsedata);

    ref.on('value', function (snapshot) {
        var donorslist = snapshot.val();
        var selecteddonorslist = [];

        for (var key in donorslist) {
            if (compatablegroups.indexOf(donorslist[key]['bloodgroup']) >= 0 && donorslist[key]['available']) {
                selecteddonorslist.push(donorslist[key]);
            }
        }
        for (var key in selecteddonorslist) {
            responsedata.donors.push({
                'name': selecteddonorslist[key]['name'],
                'mobile': selecteddonorslist[key]['contactno'],
                'distance': distance(latitude, longitude, selecteddonorslist[key]['latitude'], selecteddonorslist[key]['longitude'], 'K') + " km",
                'bgroup': selecteddonorslist[key]['bloodgroup']
            })
        }
        responsedata.donors.sort((a, b) => parseFloat(a.distance) - parseFloat(b.distance));
        response.json(responsedata);
        return;
    }, function (errorobject) {
        response.json({
            'status': 404,
            'donors': []
        });
        return;
    });
});

function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
        dist = 1;
    }
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") { dist = dist * 1.609344 }
    if (unit == "N") { dist = dist * 0.8684 }
    return dist.toFixed(2);
}


exports.ask = functions.https.onRequest((request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    var id = request.query.id
    var question = request.query.question
    var ref = db.ref('/chat');
    var res = {
        status:200,
        msg:'push successful'
    }
    var questionObject = {
        question:question,
        answer:[null],
        time:Date.now()
    }
    ref.child(id).push(questionObject,function(errorobject){
        if(errorobject){
            res.status=404
            res.msg='push failed'
            response.json(res)
            return;
        }else{
            response.json(res)
            return
        }
    });
});

exports.reply = functions.https.onRequest((request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    var uid = request.query.uid
    var qid = request.query.qid
    var reply = request.query.reply
    var docId = request.query.docid
    var ref = db.ref('/chat');
    var res = {
        status: 200,
        msg: 'push successful'
    }
    replyObj = {
        reply : reply,
        docId : docId
    }
    ref.child(uid).child(qid).child('answer').push(replyObj, function(errorobject){
        if(errorobject){
            res.status = 404
            res.msg = 'push failed'
            response.json(res)
            return
        }else{
            response.json(res)
            return
        }
    })
});


exports.getquestions = functions.https.onRequest((request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    var ref = db.ref('/chat');
    console.log(request.method)
    var res = {
        status: 200,
        chatList: []
    }
    ref.on('value', function(snapshot){
        var list = snapshot.val()
        res.chatList=list
        response.json(res)
        return

    },function(errorobject){
        res.status = 404
        response.json(res)
        return
    });
});


exports.inbox = functions.https.onRequest((request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    var uid = request.query.uid
    var ref = db.ref('/chat').child(uid);
    var res = {
        status: 200,
        msg: 'read successful',
        chatlist : []
    }

    ref.on('value', function(snapshot){
        
        var list = snapshot.val()

        for(var key in list){
            qObj = {
                question:list[key]['question'],
                answer:[]
            }
            if (list[key]['answer']){

                for (var anskey in list[key]['answer']){
                    qObj.answer.push(list[key]['answer'][anskey])
                }
            }
            res.chatlist.push(qObj)
        }

        response.json(res)
        return

    }, function(errorobject){
        
        res.status = 404
        res.msg = 'read failed'

        response.json(res)
        return
    });
    
});

exports.login = functions.https.onRequest((request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    var hospitalid = request.query.hospitalid;
    var ref = db.ref('/hospital');
    var res = {
        status: 200,
        prof : null
    }
    ref.on('value', function (snapshot) {
        var datas = snapshot.val();
        for( var key in datas){
            if(key == hospitalid){
                res.prof = datas[key];
                response.json(res);
                return;
            }
        }
        res.status = 400;
        response.json(res);
        return;

    }, function (errorobject) {
        res.status = 404;
        response.json(res);
        return;
    });
});